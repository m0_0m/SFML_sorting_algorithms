QT += core
QT -= gui

CONFIG += c++11

TARGET = visual_algorithms
CONFIG += console
LIBS += -lsfml-graphics -lsfml-window -lsfml-system
#CONFIG(release, debug|release):
#LIBS += -lwinmm -lsfml-system-s -lws2_32 -lopenal32 -lflac -lvorbisenc -lvorbisfile -lvorbis -logg -lsfml-audio-s -lgdi32 -lsfml-window-s -lfreetype -lsfml-graphics-s
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp
