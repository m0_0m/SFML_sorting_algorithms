#include <SFML/Graphics.hpp>
#include <vector>
#include <random>
#include <chrono>
#include <iostream>
#include <algorithm>
#include <stdio.h>

const int width = 640*1.5, height = 360*1.5;

void ev(sf::RenderWindow& window);
void setRand(std::vector<float>& ryad);
void bubleSort(std::vector<float>& ryad, sf::RenderWindow& window, sf::RectangleShape& rect);
void choiceSort(std::vector<float>& ryad, sf::RenderWindow& window, sf::RectangleShape& rect);
void insertSort(std::vector<float>& ryad, sf::RenderWindow& window, sf::RectangleShape& rect);
void pyramidOrHeapSort(std::vector<float>& ryad, sf::RenderWindow& window, sf::RectangleShape& rect);
void bubleSort(std::vector<float>& ryad);
void choiceSort(std::vector<float>& ryad);
void insertSort(std::vector<float>& ryad);
void pyramidOrHeapSort(std::vector<float>& ryad);

int main(){
    sf::RenderWindow window(sf::VideoMode(width, height), "Algorithms");
    window.setPosition(sf::Vector2i(360, 180));
    //unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
    //std::default_random_engine eng(seed);
    //int N = (float)eng() / eng.max() * (width);
    int N = width;
    std::vector<float>ryad(N);


    sf::RectangleShape rect;
    bool ryadRand = false;
    //rect.setFillColor(sf::Color::Magenta);
    rect.setFillColor(sf::Color(200,100,0,155));

    while (window.isOpen()){
        ev(window);
        if(ryadRand)
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::X)){
                pyramidOrHeapSort(ryad,window,rect);
                ryadRand = false;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::C)){
                choiceSort(ryad,window,rect);
                ryadRand = false;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::V)){
                insertSort(ryad,window,rect);
                ryadRand = false;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::B)){
                bubleSort(ryad,window,rect);
                ryadRand = false;
            }
            //==============================================
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
                pyramidOrHeapSort(ryad);
                ryadRand = false;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
                choiceSort(ryad);
                ryadRand = false;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::F)){
                insertSort(ryad);
                ryadRand = false;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::G)){
                bubleSort(ryad);
                ryadRand = false;
            }
        }

        window.clear();
        //draw siquence
        for (unsigned int i = 0; i < ryad.size(); ++i){
            rect.setSize(sf::Vector2f(1.f, ryad[i]));
            rect.setPosition(sf::Vector2f(i, 0.f));
            window.draw(rect);
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::R) && !ryadRand){
            setRand(ryad);
            ryadRand = true;
        }

        //window.clear();
        // draw siquence
        for (unsigned int i = 0; i < ryad.size(); ++i){
            rect.setSize(sf::Vector2f(1.f, ryad[i]));
            rect.setPosition(sf::Vector2f(i, 0.f));
            window.draw(rect);
        }

        window.display();
    }

    return 0;
}



//=======================/
//  Алгоритмы           /
//---------------------/

void bubleSort(std::vector<float>& r){
    sf::Clock timer;
    timer.restart();
    for(unsigned int j = 0; j < r.size(); ++j){
        // Тут происходит вся жара. Самый лёгкий пузырёк с конца поднимается в "верх списка",
        // а потом, внешним циклом исключается из проверки.
        for(unsigned int i = r.size() - 1; i != j; --i){
            if(r[i] < r[i-1]){
                std::swap(r[i],r[i-1]);
            }
        }
    }
    int dt = timer.getElapsedTime().asMicroseconds();
    std::cout << r.size() << " items sorted by Buble-sort in: " << dt << " mcs" << std::endl;
}

void choiceSort(std::vector<float>& r){
    sf::Clock timer;
    timer.restart();
    int buff = 0;

    for (unsigned int j = 0; j < r.size(); ++j){
        buff = j;
        // Линейно ищем самый маленький эллемент, меняем его местами с самым "первым",
        // а потом, внешним циклом исключаем из проверки.
        for(unsigned int i = j; i < r.size(); ++i){
            if(r[i] < r[buff]){
                buff = i;
            }
        }
        std::swap(r[j],r[buff]);
    }
    int dt = timer.getElapsedTime().asMicroseconds();
    std::cout << r.size() << " items sorted by Choice-sort in: " << dt << " mcs" << std::endl;
}

void insertSort(std::vector<float>& r){
    sf::Clock timer;
    timer.restart();
    // Первый эллемент считаем уже отсортированным. j - номер соседнего эл-та
    for (unsigned int j = 1; j < r.size(); ++j){
        // Для каждый отсортированный эл-т сравниваем с соседним не отсортированным.
        //      [отсортированный эл-ты], [соседний], [не соседние]
        // Если попался отсортированный i-ый эл-т < соседнего, значит перед ним (т.е. в i+1)
        // нужно поставить соседний и принять его за отсортированный.
        for(unsigned int i = j; i != 0; --i){
            if(r[i - 1] <= r[i]){
                break;
            }
            std::swap(r[i], r[i - 1]);
        }
    }
    int dt = timer.getElapsedTime().asMicroseconds();
    std::cout << r.size() << " items sorted by Insert-sort in: " << dt << " mcs" << std::endl;
}

void pyramidOrHeapSort(std::vector<float>& r){
    sf::Clock timer;
    timer.restart();
    // Вводим последовательность в кучу (бинарное дерево) и
    // выводим список максимумов из кучи.
    std::make_heap(r.begin(),r.end());
    std::sort_heap(r.begin(),r.end());
    int dt = timer.getElapsedTime().asMicroseconds();
    std::cout << r.size() << " items sorted by Heap-sort in: " << dt << " mcs" << std::endl;
}

//===============================/
//  Визуализация алгоритмов     /
//-----------------------------/

void bubleSort(std::vector<float>& r, sf::RenderWindow& window, sf::RectangleShape& rect){
    for(unsigned int j = 0; j < r.size(); ++j){
        for(unsigned int i = r.size() - 1; i != j; --i){
            if(r[i] < r[i-1]){
                std::swap(r[i],r[i-1]);
            }
        }
        ev(window);
        window.clear();
        //draw siquence
        for (unsigned int i = 0; i < r.size(); ++i){
            rect.setSize(sf::Vector2f(1.f, r[i]));
            rect.setPosition(sf::Vector2f(i, 0.f));
            window.draw(rect);
        }
        window.display();
    }
    std::cout << "bubleSort();" << std::endl;
}

void choiceSort(std::vector<float>& r, sf::RenderWindow& window, sf::RectangleShape& rect){
    int buff = 0;
    for (unsigned int j = 0; j < r.size(); ++j){
        buff = j;
        for(unsigned int i = j; i < r.size(); ++i){
            if(r[i] < r[buff]){
                buff = i;
            }
        }
        std::swap(r[j],r[buff]);
        ev(window);
        window.clear();
        //draw siquence
        for (unsigned int i = 0; i < r.size(); ++i){
            rect.setSize(sf::Vector2f(1.f, r[i]));
            rect.setPosition(sf::Vector2f(i, 0.f));
            window.draw(rect);
        }
        window.display();
    }
    std::cout << "choiceSort();" << std::endl;
}

void insertSort(std::vector<float>& r, sf::RenderWindow& window, sf::RectangleShape& rect){
    for (unsigned int j = 1; j < r.size(); ++j){
        for(unsigned int i = j; i != 0; --i){
            if(r[i - 1] <= r[i]){
                ev(window);
                window.clear();
                //draw siquence
                for (unsigned int i = 0; i < r.size(); ++i){
                    rect.setSize(sf::Vector2f(1.f, r[i]));
                    rect.setPosition(sf::Vector2f(i, 0.f));
                    window.draw(rect);
                }
                window.display();

                break;
            }
            std::swap(r[i], r[i - 1]);
        }
    }
    std::cout << "insertSort();" << std::endl;
}

void pyramidOrHeapSort(std::vector<float>& r, sf::RenderWindow& window, sf::RectangleShape& rect){
    std::vector<float> r2(r.size());
    for(int i = 0; i < r2.size(); ++i)
        r2[i]=r[i];
    std::make_heap(r2.begin(),r2.end());
    //sf::Clock timer;
    //timer.restart();
    r.clear();
    float buff = 0.f;
    int SIZE = r2.size();
    for (int i = 0; i < SIZE; ++i){
        std::pop_heap(r2.begin(), r2.end());
        buff = *(r2.end() - 1);
        r.insert(r.begin(),buff);
        //r.push_back(buff);
        r2.pop_back();
        ev(window);
        window.clear();
        //draw siquence
        for (unsigned int i = 0; i < r.size(); ++i){
            rect.setSize(sf::Vector2f(1.f, r[i]));
            rect.setPosition(sf::Vector2f(i, 0.f));
            window.draw(rect);
        }
        //draw siquence
        for (unsigned int i = 0; i < r2.size(); ++i){
            rect.setSize(sf::Vector2f(1.f, r2[i]));
            rect.setPosition(sf::Vector2f(i, 0.f));
            window.draw(rect);
        }
        window.display();
    }
    std::cout << "pyramidOrHeapSort();" << std::endl;
}

//===================================================/
// события и генерация случайной последовательности /
//-------------------------------------------------/
void setRand(std::vector<float>& r){
    unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
    std::default_random_engine eng(seed);
    for(float& it : r){
        it = (float)eng() / eng.max() * (height);
    }
}

void ev(sf::RenderWindow& window){
    sf::Event event;
    while (window.pollEvent(event)){
        if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)){
            window.close();
        }
    }
}
